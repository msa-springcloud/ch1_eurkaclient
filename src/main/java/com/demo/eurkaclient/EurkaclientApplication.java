package com.demo.eurkaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class EurkaclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurkaclientApplication.class, args);
	}

}
